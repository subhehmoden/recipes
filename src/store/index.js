import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    status: "",
    user: {},
  },
  mutations: {
    auth_request(state) {
      state.status = "loading";
    },
    auth_success(state, user) {
      state.status = "success";
      state.user = user;
    },
    auth_error(state) {
      state.status = "error";
    },
    logout(state) {
      state.status = "";
      state.user = "";
    },
  },
  actions: {
    login({ commit }, user) {
      commit("auth_request");
      if (user.email === "admin@admin.com" && user.password === "admin") {
        console.log(user);
        commit("auth_success", user);
        return "true";
      } else {
        commit("auth_error");
        return "false";
      }
    },
    register(user) {
      if (user.email && user.password) {
        //
      }
      // commit('auth_request')
      // commit('auth_error')
    },
    logout({commit}) {
      commit("logout");
      return "ok";
    }
  },
  getters: {
    isLoggedIn: (state) => !!state.status,
    authStatus: (state) => state.status,
  },
  modules: {},
});
